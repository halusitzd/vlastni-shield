#include <Adafruit_NeoPixel.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Arduino UNO
// Piny pro LED
#define LED_Green 3
#define LED_Red 4

// Piny pro LED pásek
#define LED_Strip_Pin 2
#define LED_Strip_Number 8

// Pin pro fotorezistor
#define Photo A0

// Pin pro Teplotní čidlo
#define Temp_Sensor 6

/*
// Wemos D1
// Piny pro LED
#define LED_Green D3
#define LED_Red D4

// Piny pro LED pásek
#define LED_Strip_Pin D2
#define LED_Strip_Number 8

// Pin pro fotorezistor
#define Photo A0

// Pin pro Teplotní čidlo
#define Temp_Sensor D6
*/

// Inicializace I2C LCD displeje
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Inicializace LED pásku
Adafruit_NeoPixel strip (LED_Strip_Number, LED_Strip_Pin, NEO_GRB + NEO_KHZ800);

// Inicializace teplotního čidla DS18B20
OneWire oneWire(Temp_Sensor);
DallasTemperature sensors(&oneWire);

int led = 0;
float temperatureC;
int photoValue;

void setup() {
  // Inicializace LED
  pinMode(LED_Green, OUTPUT);
  pinMode(LED_Red, OUTPUT);

  // Inicializace LED pásku
  strip.begin();
  strip.show();

  // Inicializace teplotního senzoru
  sensors.begin();

  // Inicializace LCD displeje
  lcd.begin();
  lcd.backlight();
}

void leds() {
  // Zapnutí LED
  digitalWrite(LED_Red, HIGH);
  delay(750);
  digitalWrite(LED_Red, LOW);
  digitalWrite(LED_Green, HIGH);
  delay(750);
  digitalWrite(LED_Green, LOW);
}

void display() {
  // Čtení hodnoty z fotorezistoru
  photoValue = analogRead(Photo);

  // Čtení teploty z DS18B20
  sensors.requestTemperatures();
  temperatureC = sensors.getTempCByIndex(0);

  // Zobrazení hodnot na LCD
  lcd.clear();
  lcd.setCursor(0, 0);

  lcd.print("Light: ");
  lcd.print(photoValue);
  
  lcd.setCursor(0, 1);
  lcd.print("Temp: ");
  lcd.print(temperatureC);
  lcd.print(" °C");
}

void led_strip()  {
  // Nastavení intenzity svítivosti
  strip.setBrightness(50);
  
  // Náhodné rozsvícení LED pásku
  for (int i = 0; i < LED_Strip_Number; i++) {
    strip.setPixelColor(i, strip.Color(random(0, 255), random(0, 255), random(0, 255)));
  }
  strip.show(); // Aktualizace pásku

  // Zhasnutí pásku po 1 sekundě
  delay(750);
  for (int i = 0; i < LED_Strip_Number; i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
  }
  strip.show(); // Aktualizace pásku
}

void loop() {
  leds();

  led_strip();

  display();
}