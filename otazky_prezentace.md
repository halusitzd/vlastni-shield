# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala | 12 hodin |
| jak se mi to podařilo rozplánovat | Kupodivu docela dobře|
| design zapojení | https://gitlab.spseplzen.cz/halusitzd/vlastni-shield/-/blob/main/dokumentace/schema/fritzing_halusitzd_bb.jpg |
| proč jsem zvolil tento design | |
| zapojení | https://gitlab.spseplzen.cz/halusitzd/vlastni-shield/-/blob/main/dokumentace/schema/schema_halusitzd_schem.jpg |
| z jakých součástí se zapojení skládá | LCD display s I2C sběrnicí, RGB pásek, teplotní čidlo, 2 LED a fotorezistor|
| realizace | https://gitlab.spseplzen.cz/halusitzd/vlastni-shield/-/blob/main/dokumentace/fotky/IMG_1267.jpeg |
| nápad, v jakém produktu vše propojit dohromady| Chytrá domácnost?? |
| co se mi povedlo | Překvapivě čisté spoje |
| co se mi nepovedlo/příště bych udělal/a jinak | Kód a funkčnost na Wemos D1|
| zhodnocení celé tvorby | Až na t ože shield funguje jen na Arduinu Unu tak docela dobrý 8/10 |
